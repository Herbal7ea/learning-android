package com.example.TestExamples

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals

import org.junit.Test

import org.junit.Before

//region Class to be tested

class Consumer(val dependency: Dependency) {

    var userName = ""

    fun loadData_returningResult(): String {
        var result = dependency.loadTitle()
            result.capitalize()
            result += "\n//1234"
        return result
    }


    fun loadData_sideEffects() {
        val result = dependency.getUserName()

        userName = result.split(' ')
                         .joinToString(" ") { it.capitalize() }
    }


}

//endregion

//region Dependency

interface Dependency {
    fun loadTitle(): String
    fun getUserName(): String
}

class DependencyImpl: Dependency {
    override fun loadTitle(): String {
        return "some title"
    }

    override fun getUserName(): String {
        return "some userName"
    }
}

//endregion

//region Tests

class MockExampleTest {

    //region fields

    private val json = "{ \"title\": \"Totally Useful Json\" }"

    var mockDependency: Dependency = mock {
        on { loadTitle() } doReturn json
        on { getUserName() } doReturn "kakashi hatake"
    }

    lateinit var classUnderTest: Consumer

    //endregion

    @Before
    fun setUp() {
        classUnderTest = Consumer(mockDependency)
    }

    //region loadData_returningResult

    @Test
    fun loadData_returningResult__dependency_loadTitle() {
        //Act
        classUnderTest.loadData_returningResult()

        //Assert
        verify(mockDependency).loadTitle()
    }

    @Test
    fun loadData_returningResult__alterValue() {
        //Arrange
        val expectedJson = "{ \"title\": \"Totally Useful Json\" }\n//1234"

        //Act
        val result = classUnderTest.loadData_returningResult()

        //Assert
        assertEquals(expectedJson, result)
    }

    //endregion

    //region loadData_sideEffects

    @Test
    fun loadData_sideEffects__Calls__dependency_getUserName() {
        classUnderTest.loadData_sideEffects()

        verify(mockDependency).getUserName()
//        verify(mockDependency, times(1)).getUserName()
    }

    @Test
    fun loadData_sideEffects__CapitalizeName() {
        //Arrange
        val expected = "Kakashi Hatake"

        //Act
        classUnderTest.loadData_sideEffects()

        //Assert
        assertEquals(expected, classUnderTest.userName)
    }

    //endregion
}

//endregion