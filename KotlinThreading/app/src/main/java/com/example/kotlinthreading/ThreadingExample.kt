package com.example.kotlinthreading

import android.os.AsyncTask
import com.jonbott.androidui.Helpers.Threading
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

object ThreadingExample {
    private val bag = CompositeDisposable()

    //region AsyncTask

    fun example_AsyncTask() {
        println("Starting Example01")

        val task = object: AsyncTask<String, Int, String>() {
            override fun doInBackground(vararg p0: String?): String {
                //do something useful
                println("🍩🤡 doing something funny")
                Thread.sleep(2000)
                return "AsyncTask is finished"
            }

            override fun onProgressUpdate(vararg progress: Int?) {
                if(progress == null) return
                println("🍩 in progress: ${progress[0]}")
            }

            override fun onPostExecute(result: String?) {
                println("🍩😵 onPostExecute: result = $result")
            }
        }
        task.execute("some useful arguments")

        println("finished main block of Example01")
    }

    //endregion

    //region RxJava Threading

    fun example_RxJava() {
        fakeServerCall()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                //ui stuff
            }
    }

    fun rxJavaStartSimpleThread() {
        Single.fromCallable {
            //do something
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe( { //onSuccess
            //do something else
        }, { //onError

        })
    }
    //endregion

    fun threadingHelperExample() {
        var disposable = Threading.async(task = {
            println("🦄🥋🤡 Doing something useful")
            Thread.sleep(2000)

            "Task Finished: some useful message" //implicitly returned
        }, finished = { result ->
            println("🦄😵 Handle the result:: $result")
        })

        println("🦄 Finishing Example02")
        bag.add(disposable)
    }





    fun fakeServerCall(): Single<String> {
        return Single.fromCallable {
            "final server result" //implicitly returned
        }.subscribeOn(Schedulers.io())
    }

    private fun doSomething() {
        println("Doing something useful")
    }

    fun cleanUp() {
        bag.dispose()
    }

}