package com.jonbott.androidui.Helpers

import android.os.Handler
import android.os.Looper
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

typealias Lambda<T> = () -> T
typealias VoidLambda<T> = (T) -> Unit

object Threading {

    fun handlerExample(block: Lambda<Unit>) {
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post {
            try {
                block()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun <T> async(task: Lambda<T>, finished: VoidLambda<T>? = null, onError: VoidLambda<Throwable>? = null, scheduler: Scheduler = Schedulers.io()): Disposable {
        var finished1 = finished ?: { _ -> }
        var onError1  = onError  ?: { _ -> }

        return Single.fromCallable(task)
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(finished1, onError1)
    }

}

class DisposeBag {
    val disposables = mutableListOf<Disposable>()

    fun add(disposable: Disposable) {
        disposables.add(disposable)
    }

    fun dispose() {
        disposables.forEach {
            if(!it.isDisposed) {
                it.dispose()
            }
        }
    }
}
