package com.example.kotlinthreading

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

//https://proandroiddev.com/android-coroutine-recipes-33467a4302e9

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        someTextView.text = "Starting Text"

        threadingExamples()
    }

    private fun threadingExamples() {
        ThreadingExample.example_AsyncTask()
        ThreadingExample.example_RxJava()
        ThreadingExample.threadingHelperExample()

        //What's new:
        inlineCoroutineThreading()
    }

    private fun inlineCoroutineThreading() {
        println("🔱 Thread: ${Thread.currentThread().name}")


        //lives for the life of the app
        GlobalScope.launch {
            //do something
            delay(3000)
            doSomethingIntense()
            //
            println("👻 Nice!")
        }

        //lives for the life of the activity
        ScopeManager.ioScope.launch {
            println("🧜🏻‍♀️🌈 doing something")
            delay(3000)
            //someTextView.text = "This is dangerous" //UI on backgroung thread

            ScopeManager.uiScope.launch {
                someTextView.text = "This is safe"
            }

            println("🔱 Thread: ${Thread.currentThread().name}")
        }
    }

    private fun doSomethingIntense() {
        ScopeManager.ioScope.launch {
            //do something else
        }
    }
}


object ScopeManager {
    val uiScope = MainScope()
    val ioScope = IOScope()
}

class MainScope : CoroutineScope, LifecycleObserver {

    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun destroy() = coroutineContext.cancelChildren()
}

class IOScope : CoroutineScope, LifecycleObserver {

    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun destroy() = coroutineContext.cancelChildren()
}